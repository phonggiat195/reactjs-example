import React, { Component } from 'react'

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';


export default class Detail extends Component {

    
    render() {
     
        return (
            <div>
      <GridList cellHeight={300} cols = {3} spacing = {20} >
        
        {this.props.filteredProducts.map(tile => (
          <GridListTile key={tile.id}>
            <img src={tile.img} alt={tile.title} />
            <GridListTileBar
              title={tile.title}
            />
          </GridListTile>
        ))}
      </GridList>
    </div>
        )
    }
}
