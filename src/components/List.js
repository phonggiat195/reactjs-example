import React, { Component } from 'react'
import Detail from './Detail'
import TextField from '@material-ui/core/TextField';

export default class List extends Component {

    constructor(props) {
        super(props);
        this.state ={
        products: [
                {
                  img: 'http://bmwsaigon.vn/static/img/new-models/bmw-1s-f20/bmw-118i-overview-bmwsaigon-01.jpg',
                  title: 'BMW 118i 5 cửa 2019',
                  id :1,
              },
              {
                  img: 'https://www.mercedes-benz.com.vn/vi/passengercars/mercedes-benz-cars/models/s-class/mercedes-maybach/explore/design/_jcr_content/hotspotimage/image.MQ6.12.20180926135737.jpeg',
                  title: 'Mercedes Maybach S-Klasse Saloon',
                  id :2,
              },
              {
                  img: 'https://www.mercedes-benz.com.vn/vi/passengercars/mercedes-benz-cars/models/cla/cla-coupe/explore/concept-intro/_jcr_content/contentgallerycontainer/par/contentgallery_c8fa/par/contentgallerytile_e/image.MQ6.8.20181015080339.jpeg',
                  title: 'Mercedes E-Class Saloon',
                  id :3,
              },
              {
                  img: 'https://www.mercedes-benz.com.vn/vi/passengercars/mercedes-benz-cars/models/cla/cla-coupe/explore/amg/_jcr_content/highlightcontainer/par/highlighttile_5718.MQ6.0.20181016090823.jpeg',
                  title: 'Mercedes-AMG CLA 45 Coupé',
                  id :4,
              },
              {
                  img: 'https://www.mercedes-benz.com.vn/vi/passengercars/mercedes-benz-cars/models/s-class/saloon/explore/highlights/_jcr_content/highlightcontainer/par/highlighttile_15b7.MQ6.0.20180803130603.jpeg',
                  title: 'Mercedes S-Klasse Saloon',
                  id :5,
              },
              {
                  img: 'http://bmwsaigon.vn/static/img/new-models/330imsport/1.jpg',
                  title: 'BMW 330i 2019 M Sport',
                  id :6,
              },
              {
                  img: 'http://bmwsaigon.vn/static/img/new-models/bmw-x4-g02/x4-2018-560x315.jpg',
                  title: 'BMW X4 xDrive 20i',
                  id :7,
              },
              {
                  img: 'http://bmwsaigon.vn/static/img/new-models/bmw-x5-f15/bmwx501.jpg',
                  title: 'BMW X5 xDrive35i',
                  id :8,
              },
              {
                  img: 'https://www.bmwusa.com/content/dam/bmwusa/Future_vehicles/i8/BMW_Future_i8Roadster_LP_04.jpg',
                  title: 'BMW i8 Roadster Mui trần',
                  id :9,
              }
              ],
              searchText: ''
        }
    }

    handleTextfeild = (event) => {
        console.log(event.target.value);
        this.setState({ searchText: event.target.value })
    }

    render() {

        let filteredProducts = this.state.products.filter((car) => {
            return car.title.toLowerCase().includes(this.state.searchText.toLowerCase())
        } )

        return (
            <div>
                 <div className='searchText'>
                    <TextField
                        id="standard-search"
                        style={{width:'25%', margin:30}}
                        label="Search field"
                        type="search"
                        className='search'
                        onChange = {this.handleTextfeild}
                        />
                 </div>

                    <Detail filteredProducts = {filteredProducts}/>
            </div>
        )
    }
}


