import React from 'react';
import './App.css';
import NavBar from './components/NavBar'
import List from './components/List'

function App() {
  return (
    <div className="App">
      <NavBar/>
      <List/>
    </div>
  );
}

export default App;
